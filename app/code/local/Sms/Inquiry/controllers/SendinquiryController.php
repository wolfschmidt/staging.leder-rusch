<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 22.08.2019
 * Time: 14:28
 */

class Sms_Inquiry_SendinquiryController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('root')
            ->setTemplate('page/2columns-left.phtml');
        $this->renderLayout();
    }
}