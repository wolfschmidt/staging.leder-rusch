<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 21.09.2018
 * Time: 11:59
 */

class Sms_Inquiry_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('root')
            ->setTemplate('page/1column.phtml');
        $this->renderLayout();
    }

    public function sendinquiryAction() {
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('root')
            ->setTemplate('page/2columns-left.phtml');
        $this->renderLayout();
    }

}