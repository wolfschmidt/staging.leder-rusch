<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('sms_inquiry'))//this will select your table
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Customer Id')
    ->addColumn('wishlist_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Wishlist Id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Product Id')
    ->addColumn('product_amount', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Product Amount')
    ->addColumn('comment', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Comment')//check type
    ->addColumn('is_send', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'unsigned' => false,
    ), 'Is Send')
    ->addColumn('date_sent', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'unsigned' => false,
    ), 'Date Sent')
    ->addIndex($installer->getIdxName('sms_inquiry', array('customer_id', 'product_id', 'wishlist_id')), array('customer_id', 'product_id', 'wishlist_id'));

$installer->getConnection()->createTable($table);

$installer->endSetup();
