<?php

class Sms_Inquiry_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getComment($adid)
    {
        if ($adid <> "") {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $SQL = "SELECT description FROM wishlist_item WHERE wishlist_item_id = $adid";
            $comment = $readConnection->fetchOne($SQL);
            return $comment;
        } else {
            $comment = array();
        }
        return $comment;
    }

    /**
     * @param $data
     * @param $cid
     * @param $wid
     */
    public function sendInquiry($data, $cid, $wid)
    {

        $customer = Mage::getModel('customer/customer')->load($cid);
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $abfrage = array();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $anzahl = count($value);
                $i = 0;
                foreach ($value as $_value => $_insert) {
                    $abfrage[$i][$key] = $_insert;
                    $i++;
                }

            }
        }

//        $text = "Neue Anfrage von " . $customer->getDefaultBillingAddress()->getCompany() . ", " . $customer->getFirstname() . " " . $customer->getLastname() . ", Kundennummer " . $customer->getId() . "\r\n";
        $text = "Neue Anfrage von " . $customer->getFirstname() . " " . $customer->getLastname() . ", Kundennummer " . $customer->getId() . "\r\n";
        $from = "From: Leder Rusch Online <online@leder-rusch.de>";
        $empfaenger = "info@leder-rusch.de, online@leder-rusch.de, wolf.schmidt@schmidt-medien.de";
        $betreff = "Neue Artikelanfrage";
//        $header = array(
//            'From' => 'noreply@leder-rusch.de',
//            'Reply-To' => 'noreply@leder-rusch.de',
//            'X-Mailer' => 'PHP/' . phpversion()
//        );

        $ca = count($abfrage);
        $j = 0;
        $now = date('Y-m-d');
        while ($j < $ca) {
            foreach ($abfrage[$j] as $key => $value) {
                $pid = $abfrage[$j]["product_id"];
                $amount = $abfrage[$j]["amount"];
                $comment = $abfrage[$j]["comment"];
                if ($this->proofInsert($cid, $wid, $abfrage[$j]["product_id"]) == "False") {
                    $newresult = $writeConnection->query("INSERT INTO sms_inquiry (id, customer_id, wishlist_id, product_id, product_amount, comment, is_send, date_sent)
VALUES
('', '$cid', '$wid', '$pid', '$amount', '$comment', '1', '$now')");

                    $deleteFromWishlist = $writeConnection->query("DELETE FROM wishlist WHERE wishlist_id = '$wid'");

                    $_product = Mage::getModel('catalog/product')->load($pid);
                    $text .= "Artikel " . $_product->getSku() . " " . $_product->getName() . " Anzahl: " . $amount . "\r\n " . $comment . "\r\n\r\n";
                }
            }
            $j++;
        }

        mail($empfaenger, $betreff, $text, $from);
    }

    private function proofInsert($cid, $wid, $pid)
    {

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT is_send FROM sms_inquiry WHERE product_id = $pid AND customer_id = $cid AND wishlist_id = $wid";
        $result = $readConnection->fetchOne($SQL);
        if ($result) {
            return ("True");
        } else {
            return ("False");
        }
    }

    public function checkSent($pid, $wid)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT is_send FROM sms_inquiry WHERE product_id = '$pid' AND wishlist_id = '$wid'";
        $result = $readConnection->fetchOne($SQL);
        if ($result) {
            return ("True");
        } else {
            return ("False");
        }
    }

    public function getDate($pid)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT date_sent FROM sms_inquiry WHERE product_id = $pid";
        $result = $readConnection->fetchOne($SQL);
        if ($result) {
            $date = $result;
        } else {
            $date = array();
        }
        return ($date);
    }

    public function getIsSend($cid) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_inquiry WHERE customer_id = $cid";
        $result = $readConnection->fetchAll($SQL);
        if ($result) {
            $items = $result;
        } else {
            $items = array();
        }
        return ($items);
    }

    public function deleteFromInquiry($id, $wid) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $sql = "DELETE FROM sms_inquiry WHERE product_id = '$id' AND wishlist_id = '$wid'";
        $deleteFromWishlist = $writeConnection->query($sql);
    }
}