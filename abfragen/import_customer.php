<?php
//Autor: Wolf Schmidt - Schmidt Medienservice
//für Leder Rusch GbR
//05.03.2019
//alle Rechte liegen beim Autor

$start = microtime(true);
$class = NEW SmsImportCustomer();

//ini_set('memory_limit', '4096M');
ini_set('display_errors', '1');
error_reporting(1);

require_once '../app/Mage.php';
Mage::app();

define ('DOCUMENT_ROOT', Mage::getBaseDir());
define ('DIR_IMPORT', '');
define ('FILE_IMPORT', 'Adressen.csv');
define ('LOG_FILE', 'customer_update.log');
define ('LOG_PATH', '../var/log/');

$fileRowCount = 0;
$actionCount = 0;
$logFileHandle = $class->openFile(DOCUMENT_ROOT . LOG_PATH, LOG_FILE, 'a');

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (($fileHandle = $class->openFile(DIR_IMPORT, FILE_IMPORT, 'r')) !== FALSE) {

    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $fileRowCount++;
        $class->registerCustomer($data);
        $class->setCustomerToNewsletter($data);
        $message = $fileRowCount . " - Kundennummer: " . $data[0] . "\n";
        $class->writeLogEntry($message, $logFileHandle);
        $class->printMessage($message);
        $actionCount++;
    }
    exit;
    $class->closeFile($fileHandle);
    $end = microtime(true);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen und " . $actionCount . " Aenderungen vorgenommen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsImportCustomer
{

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

    public function registerCustomer($data)
    {

        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(array(1));
        $customer->setCurrentStore(1);

        $customer->loadByEmail($data['9']);

        if (!$customer->getId()) {
            $customer->setWebsiteId(1);
            $customer->setEmail($data['9']);
            $customer->setFirstname($data[1]);
            $customer->setLastname($data[2]);
            $customer->setPassword($data[10]);
            $customer->setCreatedAt(date('Y-m-d H:i:s'));
            $customer->setGroupId(3);
            $customer->setSuffix($data[0]);
            $customer->setCustomerActivated(1);
        }
        try {
            $customer->save();
        } catch (Exception $ex) {
            Zend_Debug::dump($ex->getMessage());
        }

        // Adresse speichern
        $_custom_address = array(
            'firstname' => $data[1],
            'lastname' => $data[2],
            'street' => array(
                '0' => $data[4],
                '1' => $data[13]
            ),
            'city' => $data[6],
            'company' => $data[3],
            'postcode' => $data[5],
            'telephone' => $data[7],
            'fax' => $data[8],
        );
        $customAddress = \Mage::getModel('customer/address');
        $customAddress->setData($_custom_address)
            ->setCustomerId($customer->getId())// this is the most important part
            ->setIsDefaultBilling('1')// set as default for billing
            ->setIsDefaultShipping('1')// set as default for shipping
            ->setSaveInAddressBook('1');
        try {
            $customAddress->save();
        } catch (Exception $ex) {
            Zend_Debug::dump($ex->getMessage());
        }

        Mage::getModel('newsletter/subscriber')->subscribe($data[9]);
    }

    public function setCustomerToNewsletter($data)
    {

        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(array(1));
        $customer->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $customer->loadByEmail($data['9']);

            Mage::getModel('newsletter/subscriber')->setImportMode(true)->subscribe($data[9]);
            $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($data[9]);
            $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
            $subscriber->setStoreId(1);
            $subscriber->setFirstname($data[1]);
            $subscriber->setLastname($data[2]);
            $subscriber->setCustomerId($customer->getId());
            $subscriber->save();
        }
}