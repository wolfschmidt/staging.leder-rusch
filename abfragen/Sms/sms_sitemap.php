<?php
/**
 * Autor: Schmidt Medienservice, Wolf Schmidt
 * für Paaschburg & Wunderlich
 * 2015-05-20
 * alle Rechte liegen beim Autor
 */
ini_set("memory_limit",'256M');
ini_set('max_execution_time', 900);
require_once '/var/www/vhosts/leder-rusch.de/httpdocs/staging.leder-rusch/app/Mage.php';

UMASK(0);
Mage::app();
Mage::init();

$collection = Mage::getModel('sitemap/sitemap')->getCollection();

    foreach ($collection as $sm) {
        try {
            $sm->generateXml();
        }
        catch (Exception $e) {
            $errors[] = $e->getMessage();
        }
    }

print "Die Sitemap.xml für Leder Rusch wurde geschrieben";
